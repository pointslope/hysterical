(defproject hysterical "0.1.0-SNAPSHOT"
  :description "Experimenting with Hystrix"
  :url "http://pointslope.com/hysterical"
  :license {:name "Eclipse Public License"
            :url "http://www.eclipse.org/legal/epl-v10.html"}
  :dependencies [[org.clojure/clojure "1.5.0-beta9"]
                 [clj-http-lite "0.2.0"]
                 [com.netflix.hystrix/hystrix-clj "1.2.7"]
                 [org.slf4j/slf4j-simple "1.7.2"]]
  :main hysterical.core)
