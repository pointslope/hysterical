(ns hysterical.core
  (:require [hysterical.quotes :as hq])
  (:gen-class))

;; Never accept unbounded result sets
(def ^:dynamic *max-fetch* 10)

(defn -main
  "Get some stock quotes"
  [& args]
  (doseq [[symbol price] (hq/fetch-all-prices (take *max-fetch* args))]
    (println (str symbol ": " @price)))
  (System/exit 0)) ; see https://github.com/Netflix/Hystrix/issues/102
