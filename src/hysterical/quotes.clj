(ns hysterical.quotes
  (:require [clj-http.lite.client :as http]
            [clojure.java.io :as io]
            [clojure.string :as cstr]
            [com.netflix.hystrix.core :as hyx]))

(defn- fetch-stock-data
  "Fetches the stock data for a stock ticker symbol."
  [ticker]
  (let [url (str "http://download.finance.yahoo.com/d/quotes.csv?s="
                 ticker
                 "&f=price")]
    (-> url http/get :body (cstr/split #","))))

(defn- stock-price
  "Retrieves the price from the stock data structure."
  [stock-data]
  (str "$" (first stock-data)))

(hyx/defcommand fetch-price
  "A command for fetching the price of a stock ticker symbol.
   Returns 'Unavailable' in case of failure."
  {:hystrix/fallback-fn (constantly "Unavailable")}
  [ticker]
  (stock-price (fetch-stock-data ticker)))

(defn fetch-all-prices
  "Given a sequence of stock tickers, queues asychronous requests
   to fetch their prices and returns a map of those tickers to Futures
   containing their prices."
  [tickers]
  (apply merge
         (map (fn [ticker]
                (hash-map ticker (hyx/queue #'fetch-price ticker))) tickers)))
