# hysterical

Experimental command line app that retrieves and prints stock prices.

## Usage

    $ lein run AAPL MSFT INTC HD NCLH CAKE GE AXP AKAM ATML LNKD GRPN CRM ORCL

## License

Copyright © 2013 Point Slope, LLC

Distributed under the Eclipse Public License, the same as Clojure.
